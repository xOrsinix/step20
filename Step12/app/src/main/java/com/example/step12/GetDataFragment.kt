package com.example.step12

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentGetDataBinding
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.util.concurrent.Flow
import java.util.concurrent.TimeUnit
import kotlin.random.Random

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

class GetDataFragment : Fragment() {

    lateinit var binding:FragmentGetDataBinding
    lateinit var retrofitData:List<PostItem>

    companion object{
        var isDataGot = false
        var isObsListFormed = false
        var SIZE = 0
        val observableList:ArrayList<PostItem> = ArrayList()
        fun parse():Observable<Int>{
            return Observable.create {
                for (i in 0..SIZE) {
                    Thread.sleep(500L)
                    it.onNext(observableList[i].id)
                }
                it.onComplete()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (isObsListFormed || isDataGot)
            Toast.makeText(context,"Data was cleared. Please repeat operations",Toast.LENGTH_SHORT).show()
        isObsListFormed = false
        isDataGot = false
        super.onCreate(savedInstanceState)
    }

    override fun onPause() {
        super.onPause()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGetDataBinding.inflate(inflater)
        bNavSettings()
        binding.btGetData.setOnClickListener {
            if (isDataGot==false) {
                try {
                    val retrofitBuilder = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_URL)
                        .build()
                        .create(ApiInterface::class.java)

                    CoroutineScope(Dispatchers.IO).launch {
                        retrofitData = retrofitBuilder.getData()
                    }
                    binding.tvStatus.setText("Data acquisition was successful")
                } catch (e: Exception) {
                    binding.tvStatus.setText(e.localizedMessage)
                }
                isDataGot = true
            }
            else
            {
                Toast.makeText(context,"Data has already got", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btProcessing.setOnClickListener {
            if (isDataGot) {
                formObsList()
                isObsListFormed = true
                binding.tvStatus.setText("Data list was succesfully generated ")
            }
            else
                Toast.makeText(context,"Please press GET DATA before using this ", Toast.LENGTH_SHORT).show()
        }
        return binding.root
    }

    private fun formObsList(){
        SIZE = Random.nextInt(10,20)
        for (i in 0..SIZE){
            observableList.add(retrofitData[i])
        }
    }


    private fun bNavSettings(){
        binding.bNavGetData.selectedItemId = R.id.get_data_item
        binding.bNavGetData.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.get_data_item -> {}
                R.id.demo_item -> {
                    Navigation.findNavController(binding.root).navigate(R.id.fromGetToDemo)}
            }
            true
        }
    }
}
