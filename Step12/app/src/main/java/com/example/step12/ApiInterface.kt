package com.example.step12

import retrofit2.http.GET

interface ApiInterface {

    @GET("posts")
    suspend fun getData(): List<PostItem>
}