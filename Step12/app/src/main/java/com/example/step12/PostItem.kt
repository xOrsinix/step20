package com.example.step12

data class PostItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)