package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentDemonstrationBinding
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.kotlin.toObservable
import io.reactivex.rxjava3.schedulers.Schedulers

class DemonstrationFragment : Fragment() {

    lateinit var binding:FragmentDemonstrationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDemonstrationBinding.inflate(inflater)
        bNavSettings()
        binding.btStart.setOnClickListener {
            if (GetDataFragment.isDataGot && GetDataFragment.isObsListFormed) {
                GetDataFragment.parse()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        binding.tvText.setText("Checking post #${it}")
                    }, {
                        binding.tvText.setText(it.localizedMessage)
                    }, {
                        binding.tvText.setText("Checking complete")
                    })
            }
            else
            {
                Toast.makeText(context,"Please press GET DATA and GENERATE DATA LIST on Get Data page",Toast.LENGTH_SHORT).show()
            }
        }
        return binding.root
    }

    private fun bNavSettings(){
        binding.bNavDemo.selectedItemId = R.id.demo_item
        binding.bNavDemo.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.demo_item -> {}
                R.id.get_data_item -> {Navigation.findNavController(binding.root).navigate(R.id.fromDemoToGet)}
            }
            true
        }
    }
}

/*

val dispose = GetDataFragment.dataParse()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    binding.tvText.text = "Now I'm working with #$it person"
                },{
                    binding.tvText.text = it.localizedMessage
                }, {
                    binding.tvText.text = "Parsing complete"
                })

 */
